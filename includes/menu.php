<?php 
	session_start();
function menu(){?>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<div class="container">
			<a class="navbar-brand" href="index.php">Inicio</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" 
			aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class=" navbar-nav ml-auto">
					<?php 
						if(isset($_SESSION['user'])){
							if($_SESSION['isAdmin']){?>
								<li class="nav-item active">
									<a class="nav-link" href="admin_panel.php">Admin Panel</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="profile.php">Mi cuenta</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="login.php?salir=true">Salir</a>
								</li>
							<?php
							}
							else {?>
								<li class="nav-item active">
									<a class="nav-link text-success" href="profile.php?saldo=1"><?php echo 'Saldo: $ '.$_SESSION['saldo'] ?></a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="profile.php?carrito=1">Mi Carrito</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="profile.php">Mi cuenta</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="login.php?salir=true">Salir</a>
								</li>
							<?php	
							}
						}
						else {?>
							<li class="nav-item active">
								<a class="nav-link" href="registro.php">Registro</a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="login.php">Login</a>
							</li>
						<?php	
						}
						?>
				</ul>
			</div>
		</div>
	</nav>	
	
<?php
}
?>
