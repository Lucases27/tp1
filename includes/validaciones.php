<?php
if(!isset($_SESSION['user'])){
    header('location: ../error404.php');
}

function validaCampo($campo){
    $ok = validaCaracteres($campo);
    if(strlen($campo)<5 || strlen($campo)>10){
        $ok = false;
    }
    return $ok;
}

function validaCaracteres($campo){
    $ok = true;
    $verdura = " !\"·$%&/()=?¿^`+,´|#~€¬¡'";
    for ($i=0 ; $i<strlen($verdura); $i++) {
        for ($j = 0; $j < strlen($campo); $j++) {
            if($verdura[$i]==$campo[$j]) {
                $ok = false;
                break;
            }
        }
    }
    return $ok;
}

function validaCaracteresConEspacio($campo){
    $ok = true;
    $verdura = "!\"·$%&/()=?¿^`+,´|#~€¬¡'";
    for ($i=0 ; $i<strlen($verdura); $i++) {
        for ($j = 0; $j < strlen($campo); $j++) {
            if($verdura[$i]==$campo[$j]) {
                $ok = false;
                break;
            }
        }
    }
    return $ok;
}





?>