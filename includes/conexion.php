<?php
    if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
    // 1 - Datos de conexion.
    $usuario = "root";
    $clave = "";
    $servidor = 'localhost';
    $basededatos = 'tp1'; // <------- ACA EL NOMBRE DE TU BASE DE DATOS

    // 2 - Creamos la conexion al servidor.
    $conexion = mysqli_connect($servidor,$usuario,$clave)
    or die("No se pudo conectar al servidor.");

    // 3 - Conectamos
    $db = mysqli_select_db($conexion,$basededatos)
    or die("No se pudo conectar a la base de datos."); 
?>