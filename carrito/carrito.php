<?php
    session_start();
    include('../includes/conexion.php');
    if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
    
    if(isset($_SESSION['user'])){
        $saldo = filter_var($_SESSION['saldo'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $userLogeado = strtoupper($_SESSION['user']);
    }

    //Nos traemos todos los pedidos.
    $queryPedidos = "select * from Pedidos";
    $listaPedidos = mysqli_query($conexion,$queryPedidos)
        or die("Error en la query de pedidos.");

    //Nos traemos todos los productos.
    $query = 'select * from Productos';
    $listaProductos = mysqli_query($conexion,$query)
        or die("Error en la Query Productos.");

    // GUARDAMOS UNA LISTA TEMPORAL DE CADA CODIGO QUE EL USUARIO AGREGA AL CARRITO.
    if(isset($_GET['codigo'])){
        $codigo = intval($_GET['codigo']);
        if(isset($_SESSION['listaCodigos'])){
            $_SESSION['listaCodigos'][] = $codigo;
        }
        else{
            $array = array();
            $array[] = $codigo;
            $_SESSION['listaCodigos'] = $array;
        }
        header('location: ../index.php');
    }

    //ELIMINAMOS UN ITEM DE LA LISTA TEMPORAL.
    if(isset($_GET['eliminarTemporal'])){
        $codEliminar = intval($_GET['eliminarTemporal']);
        foreach($_SESSION['listaCodigos'] as $key=>$value){
            if($value == $codEliminar){
                unset($_SESSION['listaCodigos'][$key]);
            }
        }
        header('location: ../profile.php?carrito=1');
    }

    // RECEPCION FORMULARIO COMPRA
    if(isset($_POST['compra'])){
        if(count($_SESSION['listaCodigos'])>0){
            $total = 0.0;
            $msg = "";
            $error = false;
            $cantidades = array();
            //relacionamos codigos y cantidades respectivas.
            foreach($_SESSION['listaCodigos'] as $value){
                if(!is_numeric($_POST['cantidad'.$value]) || intval($_POST['cantidad'.$value]) <1){       
                    $error = true;
                    $msg = "3";
                }
                $cantidades[$value] = $_POST['cantidad'.$value];
            }
            while($prod  = mysqli_fetch_array($listaProductos)){
                foreach($cantidades as $cod=>$cant){
                    if($cod == $prod['Codigo']){
                        $total += $prod['Precio'] * $cant;
                        if($cant>$prod['Cantidad']){
                            $error = true;
                            $msg = '5';
                        }
                    }
                }
            }
            $_SESSION['listaFinal'] = $cantidades;
            $_SESSION['totalCompra'] = $total;
            if($error){
                header('location: ../profile.php?carrito=1&msg='.$msg);
            }
            else{
                header('location: ../profile.php?carrito=2');
            }
        }
        else{
            header('location: ../profile.php?carrito=1');
        }
    }

    if(isset($_GET['confirmarCompra'])){
        if(count($_SESSION['listaFinal'])>0){
            $msg = "";
            if(floatval($_SESSION['saldo']) >= floatval($_SESSION['totalCompra'])){
                $nPedido = 0;
                $totalFinal = 0;
                $creaPedido = "insert into PEDIDOS (NCliente, Estado) VALUES('".$_SESSION['id']."','Solicitado')";
                mysqli_query($conexion,$creaPedido);
                $buscaNPedido = "select NPedido from Pedidos where NCliente='".$_SESSION['id']."' order by NPedido ASC";
                $result1 = mysqli_query($conexion,$buscaNPedido);
                while($r = mysqli_fetch_assoc($result1)){
                    $nPedido = $r['NPedido'];
                }

                while($prod  = mysqli_fetch_array($listaProductos)){
                    foreach($_SESSION['listaFinal'] as $cod=>$cant){
                        if($cod == $prod['Codigo']){
                            $totalProducto = $cant * $prod['Precio'];
                            $creaDetalles = "insert into Pedidos_detalles (NPedido,Codigo,Producto,Cantidad,PrecioUnidad,Total)
                                Values('".$nPedido."','".$cod."','".$prod['Nombre']."','".$cant."','".$prod['Precio']."','".$totalProducto."')";
                            mysqli_query($conexion,$creaDetalles);
                            $query = "update Productos set Cantidad = Cantidad - '".$cant."' where Codigo='".$cod."'";
                            mysqli_query($conexion,$query);
                            $totalFinal += $totalProducto;
                        }
                    }
                }
                $restarSaldo = "update usuarios set saldo=saldo-'".$totalFinal."' where ID='".$_SESSION['id']."'";
                mysqli_query($conexion,$restarSaldo);
                $_SESSION['saldo'] -= $totalFinal; 
                $_SESSION['listaFinal'] = array();
                $_SESSION['totalCompra'] = 0.0;
                $_SESSION['listaCodigos'] = array();
                $msg = "4";
            }
            else{
                $msg = "2";
            }
        }
        header('location: ../profile.php?carrito=2&msg='.$msg);
    }



?>