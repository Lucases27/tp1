<?php
    include_once("includes/conexion.php");
    include_once("includes/validaciones.php");
    if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }

    if(isset($_SESSION['user'])){
        $saldo = filter_var($_SESSION['saldo'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $userLogeado = strtoupper($_SESSION['user']);
    }

    // OBTENEMOS LISTA DE PRODUCTOS DESDE LA DB
    $query = 'select * from Productos';
    $listaProductos = mysqli_query($conexion,$query)
        or die("Error en la Query Productos.");


?>