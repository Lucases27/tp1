<!DOCTYPE htlm>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.10.4/mercadolibre/favicon.svg" rel="icon" data-head-react="true">
	<script src="JS/scripts.js"></script>
    <title>Mi cuenta</title>
	<?php include("includes/menu.php");?>
	<link rel="stylesheet" href="CSS/styles.css">
	<!-- OBTENEMOS LOS MENSAJES DE ERROR/EXITO -->
	<?php 					
		// SI NO ESTA LOGEADO NO LO DEJAMOS INGRESAR.
		if(!isset($_SESSION['user'])){
			header("Location: index.php");
		}
        $mensaje="";
		$mensaje2="";
		$mensaje3="";
        if(isset($_GET["modificar"])){
            $msg = $_GET["modificar"];
            switch ($msg) {
                case "error1": $mensaje = "<h5 class='text-success'>Datos modificados!</h5>";
					break;
				case "error2": $mensaje = "<h5 class='text-danger'>El email ya se encuentra registrado.</h5>";
					break;
				case "error3": $mensaje = "<h5 class='text-danger'>La contraseña es incorrecta!</h5>";
					break;
				case "error4": $mensaje = "<h5 class='text-danger'>Las contraseñas deben de ser iguales.</h5>";
					break;
				case "error5": $mensaje = "<h5 class='text-danger'>El email ingresado es invalido.</h5>";
					break;
                case "error6": $mensaje = "<h5 class='text-danger'>Los campos deben tener entre 5 y 10 caracteres únicamente alfanumericos.</h5>";
					break;  
                default:
                    break;
			}
		}
		if(isset($_GET["saldo"])){
			$msg = $_GET["saldo"];
			switch ($msg) {
				case "ok": $mensaje = "<h5 class='text-success'>Saldo transferido correctamente!</h5>";
					break;
				case "error0": $mensaje = "<h5 class='text-danger'>No tienes suficiente saldo.</h5>";
					break; 	
				case "error1": $mensaje = "<h5 class='text-danger'>El saldo debe ser mayor a 0.</h5>";
					break; 
				case "error2": $mensaje = "<h5 class='text-danger text-center'>El usuario no existe.</h5>";
					break;
				case "error3": $mensaje = "<h5 class='text-danger'>Los campos deben tener entre 5 y 10 caracteres únicamente alfanumericos.</h5>";
					break;
				case "error4": $mensaje = "<h5 class='text-danger'>Contraseña incorrecta.</h5>";
					break;	
				case "error5": $mensaje = "<h5 class='text-danger'>No tiene sentido transferir dinero a vos mismo.</h5>";
					break;
				case "errorCarga": $mensaje2 = "<h5 class='text-danger'>El saldo debe ser mayor que 0.</h5>";
					break;	
				case "okCarga": $mensaje2 = "<h5 class='text-success'>Saldo cargado correctamente.</h5>";
					break;		
				default:
					break;
			}
		}
		if(isset($_GET["carrito"])){
			if(isset($_GET["msg"])){
				$msg = $_GET["msg"];
				switch ($msg) {
					case "1": $mensaje3 = "<h5 class='text-success'>Cantidad agregada.</h5>";
						break;	
					case "2": $mensaje3 = "<h5 class='text-danger'>Tu saldo es insuficiente para realizar la compra. Debe realizar una recarga.</h5>";
						break;	
					case "3": $mensaje3 = "<h5 class='text-danger'>Cantidad incorrecta, debe ser numerica y mayor que 0.</h5>";
						break;	
					case "4": $mensaje3 = "<h5 class='text-success'>Compra realizada con exito!</h5>";
						break;
					case "5": $mensaje3 = "<h5 class='text-danger'>No hay suficientes unidades disponibles de algun producto!</h5>";
						break;		
					default:
						break;
				}
			}
        }
    ?>
</head>
<body>
	<header>
		<?php menu(); ?>
	</header>
	<div>
		<div class="alert alert-warning" >
			<h5 class="text-center">Perfil de Usuario.</h5>
		</div>
	</div>
	<section class="container-fluid">
		<div class="row d-flex">
			<div class="col-4">
				<div class="container navbar-light p-4" style="background-color: #e3f2fd;"> 
					<h5 class="text-center"> Menu Principal </h5>
					<a class="nav-link" href="profile.php?modificar=1">Datos de Usuario </a>
					<a class="nav-link" href="profile.php?saldo=1"> Tu Saldo </a>
					<a class="nav-link" href="profile.php?carrito=1"> Carrito de Compras</a>
					<a class="nav-link" href="profile.php?historial=1"> Historial de Compras</a>
				</div>			
			</div> 
			<?php 
					if(isset($_GET['modificar'])){
						include_once('profile/modificar_datos.php');
					}
					else if(isset($_GET['historial'])){
						if($_GET['historial']=='2'){
							include_once('profile/historial_detalles.php');
						}
						else{
							include_once('profile/historial_compras.php');
						}
					}
					else if(isset($_GET['saldo'])){
						include_once('profile/saldo.php');
					}
					else if(isset($_GET['carrito'])){
						if($_GET['carrito']=='2'){
							include_once('profile/carrito_confirmacion.php');
						}
						else{
							include_once('profile/carrito_productos.php');
						}
					}
					else{
					echo "<div class='col-4'>
							<div class='container navbar-light p-4' style='background-color: #e3f2fd;'> 
								<h5 class='text-center'> Bienvenido ".  ucwords(strtolower($_SESSION['user'])) ."</h5>
							</div>			
						</div>";		
					}
				?>
		</div>
	</section>
</body>
</html>