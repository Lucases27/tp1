$(function () {
    $(".js-show").each(function () {
      var $pass = $(this).find(">.js-pass");
      var $input = $(this).find(".js-check");
  
      $input.change(function () {
        if ($(this).prop("checked")) {
          $pass.attr("type", "text");
        } else {
          $pass.attr("type", "password");
        }
      });
    });
  });