<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
<?php
    if(!isset($_SESSION['isAdmin'])){
        header("Location: ../error404.php");
    }
    else {
        if(!$_SESSION['isAdmin']){
            header("Location: ../error404.php");
        }
    }
?>	
<div class="col-3 ml-n3 mt-n1">
	<div class="container navbar-light mr-2 row d-flex justify-content-center align-items-center" style="background-color: #e3f2fd;">
		<h5 class="col-12 mt-3 ml-3 text-center">Agregar nuevo producto.</h5> 
		<!-- MENSAJE DE EXITO/ERROR -->
		<div class="col-12 text-center form-group mb-0 mt-2">
			<h5 class="text-success">
                <?php echo $msg; ?>
			</h5>
		</div>
		<form action="admin/admin_productos_sql.php" method="POST">
			<div class="form-group mt-2">
				<label>Nombre: <input class="form-control" type="text" name="nombre" require placeholder="Nombre del producto"/></label>
			</div>
			<div class="form-group">
				<label>Precio<input class="form-control" type="text" name="precio" require placeholder="Precio del producto"/></label>
			</div>
			<div class="form-group">
				<label>Cantidad:<input class="form-control" type="text" name="cantidad" require  placeholder="Cantidad disponible"/></label>
			</div>
			<input type="hidden" value="1" name="nuevoProducto">
			<button type="submit" class="btn btn-primary form-control mb-4">Agregar Producto</button>
		</form>
	</div>
</div>