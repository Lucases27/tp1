<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
<?php
	if(!isset($_SESSION['isAdmin'])){
		header("Location: ../error404.php");
	}
	else {
		if(!$_SESSION['isAdmin']){
			header("Location: ../error404.php");
		}
	}
	include("admin_panel_sql.php");
?>	
<div class="col-9 ml-n5 mt-n5 p-5">
	<div class="container navbar-light  p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
	                    <td>ID cliente</td>
	                    <td>Username</td>
	                    <td>Nick</td>
	                    <td>Email</td>
	                    <td>Saldo</td>
	                    <td>Roll</td>
	                    <td>Estado</td>
	                </tr>
	            </thead>
           		<tbody>
                    <?php
                        while($d = mysqli_fetch_array($usersList)){
                        	echo "<tr>";
                            echo "<td>".$d["ID"]."</td>";
                            echo "<td>".$d["Usuario"]."</td>";
                            echo "<td>".$d["Nick"]."</td>";
                            echo "<td>".$d["Email"]."</td>";
                            echo "<td>".$d["Saldo"]."</td>";
                            if($d['isAdmin']){
                                echo "<td class='text-primary'>Administrador</td>";
                            }
                            else{
                                echo "<td class='text-info'>Cliente</td>";
                            }
                            if($d['Activo']){
                                echo "<td class='text-primary text-center'>Activo</td>";
                                echo "<td class='text-center'><a class='btn btn-outline-danger btn-sm' href='admin_panel_sql.php?bloquear=".$d['ID']."'>Bloquear</a></td>";
                            }
                            else{
                                echo "<td class='text-danger text-center'>No Activo</td>";
                                echo "<td class='text-center'><a class='btn btn-outline-success btn-sm' href='admin_panel_sql.php?desbloquear=".$d['ID']."'>Desbloquear</a></td>";
							}
							echo "</tr>";
                        }
                    ?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>