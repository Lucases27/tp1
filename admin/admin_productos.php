<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
<?php
    if(!isset($_SESSION['isAdmin'])){
        header("Location: ../error404.php");
    }
    else {
        if(!$_SESSION['isAdmin']){
            header("Location: ../error404.php");
        }
    }
    include("admin_panel_sql.php");
?>	
<div class="col-6 ml-n3 mt-n1">
	<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
	                    <td>Codigo</td>
	                    <td>Nombre</td>
	                    <td>Precio</td>
	                    <td>Cantidad</td>
	                    <td><a class="btn btn-warning btn-sm" href="admin_panel.php?productos=1&opt=1&nuevo=1">Nuevo Producto</a></td>
	                </tr>
	            </thead>
           		<tbody>
                   <?php
                        while($d = mysqli_fetch_array($listaProductos)){
                        	echo "<tr>";
                            echo "<td>".$d["Codigo"]."</td>";
                            echo "<td>".$d["Nombre"]."</td>";
                            echo "<td>".$d["Precio"]."</td>";
                            echo "<td>".$d["Cantidad"]."</td>";
                            echo "<td class='text-center'>
							<a class='btn btn-outline-primary btn-sm' href='admin_panel.php?productos=2&opt=2&prod=".$d['Codigo']."'>Modificar</a>
							<a class='btn btn-outline-primary btn-sm' href='admin/admin_productos_sql.php?opt=3&prod=".$d['Codigo']."&eliminar=1'>Eliminar</a>
						    </td>";
							echo "</tr>";
                        }
                    ?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>