<?php
    session_start();
    if(!isset($_SESSION['isAdmin'])){
        header("Location: ../error404.php");
    }
    else {
        if(!$_SESSION['isAdmin']){
            header("Location: ../error404.php");
        }
    }
    include("../includes/conexion.php");
    include("../includes/validaciones.php");


    
    if(isset($_GET['eliminar'])){
        $id = $_GET['prod'];
        $query = "delete FROM Productos WHERE Codigo=".$id;
        mysqli_query($conexion,$query);
        header("location: ../admin_panel.php?productos=eliminado&opt=3");
    }
    if(isset($_POST['nuevoProducto'])){
        $nombre = filter_var($_POST['nombre'],FILTER_SANITIZE_STRING);
        $precio = filter_var($_POST['precio'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $cantidad = filter_var($_POST['cantidad'], FILTER_SANITIZE_NUMBER_INT);
        $nombre = trim(strtolower($nombre));
        $precio = floatval($precio);
        $cantidad = intval($cantidad);


        $query = "select Nombre as nom from Productos where Nombre='$nombre'";
        $result = mysqli_query($conexion,$query);
        while($a = mysqli_fetch_assoc($result)){
            $existe = $a["nom"];
        }
        if($existe){
            $msg = "1";
        }
        else{
            if(validaCaracteresConEspacio($nombre) && strlen($nombre)>4){
                if($precio && $cantidad >0){
                    $query = "insert into Productos (Nombre,Precio,Cantidad) values('$nombre','$precio','$cantidad')";
                    mysqli_query($conexion,$query);
                    $msg = "2";
                }
                else{
                    $msg = "3";
                }
            }
            else{
                $msg = "4";
            }
        }
        header("location: ../admin_panel.php?productos=nuevo&opt=1&msg=".$msg);
    }

    if(isset($_POST['modificar'])){
        $nombre = filter_var($_POST['nombre'],FILTER_SANITIZE_STRING);
        $precio = filter_var($_POST['precio'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $cantidad = filter_var($_POST['cantidad'], FILTER_SANITIZE_NUMBER_INT);
        $nombre = trim(strtolower($nombre));
        $precio = floatval($precio);
        $cantidad = intval($cantidad);
        
        if(isset($_POST['codigo'])){
            $codigo = intval($_POST['codigo']);
            if(filter_var($codigo, FILTER_SANITIZE_NUMBER_INT)){
                if (validaCaracteresConEspacio($nombre)) {
                    if($precio > 0) {
                        if($cantidad >= 0) {
                            $query1 = "select Nombre from Productos where nombre='$nombre' and Codigo != '$codigo'";
                            $result = mysqli_query($conexion,$query1);
                            while($r = mysqli_fetch_assoc($result)){
                                $existe = $r['Nombre'];
                            }
                            if(!$existe){
                                $query = "update Productos set Nombre='$nombre', Precio='$precio', Cantidad='$cantidad' where Codigo='$codigo'";
                                mysqli_query($conexion,$query);
                                $msg = "6";
                            }
                            else{
                                $msg = "8";
                            }
                        }
                        else {
                            $msg = "5"; 
                        }
                    }
                    else {
                        $msg = "3";
                    }
                }
                else {
                $msg = "4";  
                }
            }
            else{
                $msg = "7";
            }
        }
        else{
            $msg = "7";
            header("location: ../admin_panel.php?productos=modificar&opt=2&msg=".$msg);
        }
        header("location: ../admin_panel.php?productos=modificar&prod".$id."&opt=2&msg=".$msg);
    }






?>