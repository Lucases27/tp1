<!-- SI NO ESTA LOGEADO, REDIRIGE A LA PAGINA DE ERROR. -->
<?php
	include_once('historial/historial.php');
	if(!isset($_SESSION['isAdmin'])){
		header("Location: ../error404.php");
	}
	else {
		if(!$_SESSION['isAdmin']){
			header("Location: ../error404.php");
		}
	}
?>
<div class="col-6 ml-n3 mt-n1">
	<?php 
		if($existenPedidosAdmin>0){
			echo "<div class='font-italic text-center'>
					<h6>Registro de pedidos totales.</h6>
				</div>";
		}
		else{
			echo "<div class='font-italic text-center mt-3 mb-3'>
					<h6>No hay pedidos registrados en la base de datos.</h6>
				</div>";
		}
    ?>
	<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
						<td>Nº de Pedido</td>
						<td>Nº de Cliente</td>
	                    <td>Fecha de compra</td>
	                    <td>Estado</td>
	                </tr>
	            </thead>
           		<tbody>
				   <?php
						while($d = mysqli_fetch_array($pedidosTodos)){
							echo "<tr>";
							echo "<td class='text-center'>".$d["NPedido"]."</td>";
							echo "<td class='text-center'>".$d["NCliente"]."</td>";
							echo "<td class='text-center'>".$d["Fecha"]."</td>";
							echo "<td class='text-center'>".$d["Estado"]."</td>";
							echo "<td class='text-center'>
												<a class='btn btn-outline-primary btn-sm' href='admin_panel.php?pedidos=4&npedido=".$d['NPedido']."&ncliente=".$d['NCliente']."'>Detalles</a>
											</td>";
							echo "<tr>";
						}
						?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>


