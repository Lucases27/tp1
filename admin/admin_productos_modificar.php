<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
<?php
    if(!isset($_SESSION['isAdmin'])){
        header("Location: ../error404.php");
    }
    else {
        if(!$_SESSION['isAdmin']){
            header("Location: ../error404.php");
        }
    }
?>	
<div class="col-3 ml-n3 mt-n1">
	<div class="container navbar-light mr-2 row d-flex justify-content-center align-items-center" style="background-color: #e3f2fd;">
		<h5 class="col-12 mt-3 text-center">Modificar producto.</h5> 
		<!-- MENSAJE DE EXITO/ERROR -->
		<div class="col-12 text-center form-group mb-0 mt-2">
			<h5>
				<?php echo $msg; ?>
			</h5>
		</div>
		<!-- FORMULARIO MODIFICAR PRODUCTO-->
		<form action="admin/admin_productos_sql.php" method="POST">
			<div class="form-group mt-2">
				<label>Codigo:<input class="form-control" readonly type="text" name="codigo" value="<?php echo $_GET['prod']?>" placeholder="<?php echo $_GET['prod']?>"/></label>
			</div>
			<div class="form-group">
				<label>Nombre: <input class="form-control" type="text" name="nombre" required placeholder="Nombre del producto"/></label>
			</div>
			<div class="form-group">
				<label>Precio<input class="form-control" type="text" name="precio" required placeholder="Precio del producto"/></label>
			</div>
			<div class="form-group">
				<label>Cantidad:<input class="form-control" type="text" name="cantidad" required placeholder="Cantidad del producto"/></label>
			</div>
			<input type="hidden" value="1" name="modificar">
			<button type="submit" class="btn btn-primary form-control mb-4">Modificar</button>
		</form>
	</div>
</div>