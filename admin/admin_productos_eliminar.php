<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
<?php
    if(!isset($_SESSION['isAdmin'])){
        header("Location: ../error404.php");
    }
    else {
        if(!isset($_SESSION['isAdmin'])){
            header("location: ../error404.php");
        }
    }
?>	
<div class="col-3 ml-n3 mt-n1">
    <div class="container navbar-light mr-2 row d-flex justify-content-center align-items-center" style="background-color: #e3f2fd;">
        <h5 class="mt-3 ml-3 text-center">Eliminar Producto.</h5> 
        <!-- MENSAJE DE EXITO/ERROR -->
        <div class="col text-center form-group mb-0 mt-2">
            <h5 class="text-success">
                <?php echo $msg; ?>
            </h5>
        </div>
    </div>
</div>
