<!DOCTYPE htlm>
<html lang="es">
<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.10.4/mercadolibre/favicon.svg" rel="icon" data-head-react="true">
    <link rel="stylesheet" href="CSS/styles.css">
	<title>Admin Panel</title>
	<?php include("includes/menu.php"); ?>
</head>

<body>
	<!-- SI NO ES ADMIN, REDIRIGE A INDEX. -->
	<?php
		if(!isset($_SESSION['isAdmin'])){
			header("Location: error404.php");
		}
		else {
			if(!$_SESSION['isAdmin']){
				header("Location: error404.php");
			}
		}
		$msg = "";
		if(isset($_GET['msg'])){
			if($_GET['msg']=='1'){
				$msg = "<p class='text-danger'>El producto ya existe.</p>";
			}
			if($_GET['msg']=='2'){
				$msg = "<p class='text-succes'>El producto fue agregado.</p>";
			}
			if($_GET['msg']=='3'){
				$msg = "<p class='text-danger'>El precio no puede ser 0.</p>";
			}
			if($_GET['msg']=='4'){
				$msg = "<p class='text-danger'>Nombre invalido.</p>";
			}
			if($_GET['msg']=='5'){
				$msg = "<p class='text-danger'>La cantidad no puede ser negativa.</p>";
			}
			if($_GET['msg']=='6'){
				$msg = "<p class='text-succes'>El producto fue modificado.</p>";
			}
			if($_GET['msg']=='7'){
				$msg = "<p class='text-danger'>Seleccione un producto primero.</p>";
			}
			if($_GET['msg']=='8'){
				$msg = "<p class='text-danger'>El nombre del producto ya existe.</p>";
			}
		}


	?>	
	<header>
		<?php menu(); ?>
	</header>
	<div>
		<div class="alert alert-warning" >
			<h5 class="text-center">Panel de Administrador.</h5>
		</div>
	</div>
	<section class="container-fluid">
		<div class="row d-flex">
			<div class="col-3">
				<div class="container navbar-light" style="background-color: #e3f2fd;"> 
					<h5>Menu de Administrador</h5>
					<a class="nav-link" href="admin_panel.php?usuarios=1">Ver Usuarios</a>
					<a class="nav-link" href="admin_panel.php?productos=2">Ver Productos</a>
					<a class="nav-link" href="admin_panel.php?pedidos=3">Ver Pedidos</a>
				</div>			
			</div>
			<?php
				if(isset($_GET['usuarios'])){
					include_once('admin/admin_usuarios.php');
				}
				if(isset($_GET['productos'])){
					if($_GET['productos']=='eliminado'){
						$msg = "Producto eliminado!";
					}
					include_once('admin/admin_productos.php');
				}
				if(isset($_GET['pedidos'])){
					if($_GET['pedidos']=='4'){
						include_once('admin/admin_historial_detalles.php');
					}
					else{
						include_once('admin/admin_historial_compras.php');
					}
				}
				if(isset($_GET['opt'])){
					if($_GET['opt']=='1'){
						include_once('admin/admin_productos_nuevo.php');
					}
					if($_GET['opt']=='2'){
						include_once('admin/admin_productos_modificar.php');
					}
					if($_GET['opt']=='3'){
						include_once('admin/admin_productos_eliminar.php');
					}
				}
			?>
		</div>
	</section>
</body>
</html>