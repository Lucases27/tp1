<!-- SI NO ESTA LOGEADO, REDIRIGE A LA PAGINA DE ERROR. -->
<?php
	include_once('historial/historial.php');

	if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
?>
<div class="col-6 ml-n3 mt-n1">
	<?php 
			echo "<div class='font-italic text-center'>
					<h6>Detalles del pedido Nº:".$_GET['npedido']."</h6>
				</div>";
    ?>
	<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
						<td>Codigo Producto</td>
	                    <td>Nombre Producto</td>
                	    <td>Cantidad</td>
	                    <td>Precio unidad</td>
	                    <td>Total</td>
	                </tr>
	            </thead>
           		<tbody>
				   <?php
				   		$total = 0.0;
						while($d = mysqli_fetch_array($misDetalles)){
							if($_GET['npedido'] == $d['NPedido']){
								echo "<tr>";
								echo "<td class='text-center'>".$d["Codigo"]."</td>";
								echo "<td class='text-center'>".$d["Producto"]."</td>";
								echo "<td class='text-center'>".$d["Cantidad"]."</td>";
								echo "<td class='text-center'>$ ".$d["PrecioUnidad"]."</td>";
								echo "<td>$ ".$d["Total"]."</td>";
								echo "<tr>";
								$total +=$d["Total"];
							}
						}
						?>
	            </tbody>
	        </table>
			<div class="text-right mr-4 mt-2">
			    <h6><?php echo "TOTAL $ ".$total; ?></h6>
			</div>
	    </div>
		<div class=" pb-2 mr-2 text-right">
			<a class="btn btn-outline-primary btn-sm text-right" href="profile.php?historial=1">Volver</a>
		</div>
	</div>
</div>


