<!-- SI NO ESTA LOGEADO, REDIRIGE A LA PAGINA DE ERROR. -->
<?php
	include_once('carrito/carrito_sql.php');

	if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
?>
<div class="col-6 ml-n3 mt-n1">
    <?php 
        //aca preguntamos si hay productos en el carrito
		if(sizeOf($_SESSION['listaCodigos'])>0){
			echo "<div class='font-italic text-center'>
					<h6>Lista de productos en tu carrito</h6>
				</div>";
		}
		else{
			echo "<div class='font-italic text-center mt-3 mb-3'>
					<h6>No hay ningun producto cargado al carrito. <a href='index.php'>Ver Productos!</a></h6>
				</div>";
		}
    ?>
	<!--  FORMULARIO -->
	<form action="carrito/carrito.php" method="POST">
		<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
		    <div class="table-responsive">
		        <table class="table table-bordered table-sm table-hover mb-0">
		            <thead>
		                <tr style="background:#003325;color:white" class="text-center">
		                    <td>Producto</td>
		                    <td>Precio</td>
		                    <td>Cantidad</td>
		                </tr>
		            </thead>
	           		<tbody>
		                <?php
							while($d = mysqli_fetch_array($listaProductos)){
								foreach($_SESSION['listaCodigos'] as $codigo){
									if($codigo == $d['Codigo']){
										echo "<tr>";
										echo "<td>".$d["Nombre"]."</td>";
										echo "<td class='text-center'>".$d["Precio"]."</td>";
										echo "<td class='text-center'><input type='text' name='cantidad".$d['Codigo']."' value='1' placeholder='Cantidad'></td>";
										echo "<td class='text-center'>
													<a class='btn btn-outline-primary btn-sm' href='carrito/carrito.php?eliminarTemporal=".$d['Codigo']."'>Eliminar</a>
												</td>";
										echo "<tr>";
									}
								}
							}
						?>
		            </tbody>
		        </table>
		    </div>
		</div>
		<div>
			<p>Tolal: $</p>
		</div>
 		<!-- MENSAJE DE EXITO/ERROR -->
		<div class="text-center mb-3 p-0">
			<h5 class='text-danger'>
				<?php echo $mensaje3; ?>
			</h5>
		</div>
		<div class="text-right mt-4 p-0">
			<input type="hidden" name="compra" value="1">
	    	<button type="submit" class="btn btn-primary form-control">Comprar!</button>
	    </div>
    </form>
</div>