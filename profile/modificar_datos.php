<div class="col-4">
    <div class="container" style="background-color: #e3f2fd;">        
        <div class="">
            <h5 class="text-center pt-3"> Modificar Datos </h5>				
        </div>
        <!-- FORMULARIO DE MODIFICACIÓN DE DATOS DE USUARIO -->
        <div class="row d-flex justify-content-center pb-3">
            <!-- MENSAJE DE EXITO/ERROR AL MODIFICAR -->
            <div class="col-12 form-group mb-2 mt-3 text-center">
                <?php echo $mensaje; ?>
            </div>
            <form action="profile/modificar_datos_sql.php" method="POST">
                <div class="col-12 form-group mt-3 mb-0">
                    <label>Usuario:
                        <input readonly class="form-control " type="text" name="user" 
                            id="user" value="<?php echo $_SESSION['user']?>"/>
                    </label>
                </div>
                <div class="col-12 form-group  mb-0">
                    <label>Nick: 
                        <input class="form-control" type="text" name="nick" placeholder="Nuevo Nick"
                            required min="5" max="10" value="<?php echo $_SESSION['nick']?>"/>
                    </label>
                </div>
                <div class="col-12 form-group mb-0">
                    <label>Email: <input class="form-control" type="email" name="email" placeholder="Email" mix="8" max="60"
                        value="<?php echo $_SESSION['email']?>"/></label>
                </div>
                <div class="col-12 form-group  mb-0 js-show">
                    <label>Contraseña Actual: 
                        <input class="form-control" type="password" name="pass" placeholder="Contraseña Actual" 
                        required min="5" max="10" value="<?php echo $_SESSION['pass']?>"/>
                    </label>
                    
                </div>
                <div class="col-12 form-group mb-0">
                    <label>Nueva Contraseña: 
                        <input class="form-control" type="password" name="pass1" placeholder="Nueva Contraseña" required min="5" max="10"/>
                    </label>
                </div>
                <div class="col-12 form-group mb-0">
                    <label>Repetir Contraseña: 
                        <input class="form-control" type="password" name="pass2" placeholder="Repetir Contraseña" required min="5" max="10"/>
                    </label>
                </div>
                <button type="submit" class="btn btn-primary mt-2 mb-3 form-control">Modificar!</button>
            </form>
        </div>
    </div>
</div>
<div class="col-4">
    <div class="container" style="background-color: #e3f2fd;">
        <h5 class="text-center"></h5>
    </div>
</div>