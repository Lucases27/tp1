<!-- SI NO ESTA LOGEADO, REDIRIGE A LA PAGINA DE ERROR. -->
<?php
	include_once('carrito/carrito_sql.php');

	if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
?>
<div class="col-6 ml-n3 mt-n1">
<h5>Confirme su compra</h5>
	<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
	                    <td>Producto</td>
	                    <td>Precio</td>
	                    <td>Cantidad</td>
	                    <td>Total $</td>
	                </tr>
	            </thead>
           		<tbody>
				   <?php
				   		if(isset($_SESSION['listaFinal'])){
							while($d = mysqli_fetch_array($listaProductos)){
								foreach($_SESSION['listaFinal'] as $codigo=>$cantidad){
									if($codigo == $d['Codigo']){
										echo "<tr>";
										echo "<td>".$d["Nombre"]."</td>";
										echo "<td class='text-center'>".$d["Precio"]."</td>";
										echo "<td class='text-center'>".$cantidad."</td>";
										echo "<td class='text-center'>".$d["Precio"]*$cantidad."</td>";
										echo "<tr>";
									}
								}
							}
						   }
						?>
	            </tbody>
	        </table>
	    </div>
	</div>
	<div class="text-right mr-3">
		<p>Tolal: $<?php echo $_SESSION['totalCompra'];?></p>
	</div>
	<!-- MENSAJE DE EXITO/ERROR -->
	<div class="text-center mb-3">
			<h5 class='text-danger'>
				<?php echo $mensaje3; ?>
			</h5>
	</div>
	<div>
		<a class="btn btn-outline-primary btn-sm" href="carrito/carrito.php?confirmarCompra=1">Confirmar compra</a>
		<a class="btn btn-outline-primary btn-sm text-right" href="profile.php?carrito=1">Volver</a>
	</div>
</div>


