
<div class="col-4">
    <div class="container" style="background-color: #e3f2fd;">
        <h5 class="text-center p-4 "> Tu Saldo </h5>
        <div class="mt-3">
            <p>Tu saldo actual es $: <span class="text-success"><?php echo $_SESSION['saldo']?></span></p>
        </div>
        <div class="col text-center mt-3 text-danger">
            <!-- MENSAJE DE EXITO/ERROR AL TRANSFERIR -->
            <?php echo $mensaje2; ?>
        </div>
        <!--FORMULARIO CARGAR SALDO-->
        <form action="profile/saldo_cargar_sql.php" method="POST">
            <div class="col form-group mt-3 mb-0">
                <label>Cargar saldo: 
                    <input class="form-control" type="text" name="saldo"/>
                </label>
            </div>
            <div class="col form-group  mb-0">
                <label>Metodo de pago:
                    <select name="metodopago">
                        <option value="mercadopago">Mercado Pago</option>
                        <option value="paypal">PayPal</option>
                        <option value="credito">Tarjeta de credito</option>
                        <option value="efectivo">Efectivo/Debito</option>
                    </select>
                </label>
            </div>
            <button type="submit" class="btn btn-primary mt-2 mb-3 form-control">Cargar Saldo</button>
        </form>
    </div>
</div>

<div class="col-4">
	<div class="container navbar-light" style="background-color: #e3f2fd;">
        <div class="">
            <h4 class="text-center p-4"> Transferir Saldo </h4>				
        </div>
        <div>
            <h5 class="p-3">En esta seccion usted podrá transferir saldo a otros usuarios.</h5>
        </div>
        <div class="col text-center mt-3">
            <!-- MENSAJE DE EXITO/ERROR AL TRANSFERIR -->
             <?php echo $mensaje; ?>
         </div>
        <div class="row d-flex justify-content-center pb-3 ">
            <!--FORMULARIO TRANSFERIR SALDO-->
            <form action="profile/saldo_transferir_sql.php" method="POST">
                <div class="col form-group mt-3 mb-0 d-flex">
                    <label class="align-items-center">Usuario:
                        <input class="form-control " type="text" name="user" 
                            id="user" placeholder="Usuario a transferir dinero"/>
                    </label>
                </div>
                <div class="col form-group  mb-0 d-flex">
                    <label class="align-items-center">Contraseña: 
                        <input class="form-control" type="password" name="pass" placeholder="Ingresa tu contraseña" 
                        required min="5" max="10"/>
                    </label>
                </div>
                <div class="col form-group  mb-0 d-flex">
                    <label class="align-items-center"> Cantidad: 
                        <input class="form-control" type="text" name="saldo" placeholder="Saldo a transferir" required/>
                    </label>
                </div>
                <button type="submit" class="btn btn-primary mt-2 mb-3 form-control">Transferir dinero</button>
            </form>
        </div>
    </div>
</div>
			