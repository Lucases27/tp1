<!-- SI NO ESTA LOGEADO, REDIRIGE A LA PAGINA DE ERROR. -->
<?php
	include_once('historial/historial.php');


	if(!isset($_SESSION['user'])){
        header('location: ../error404.php');
    }
?>
<div class="col-6 ml-n3 mt-n1">
	<?php 
		if($existenPedidos>0){
			echo "<div class='font-italic text-center'>
					<h6>Llevá el control de todas tus compras</h6>
				</div>";
		}
		else{
			echo "<div class='font-italic text-center mt-3 mb-3'>
					<h6>Hasta ahora no has realizado ninguna compra =(</h6>
				</div>";
		}
    ?>
	<div class="container navbar-light mr-2 p-0" style="background-color: #e3f2fd;"> 
	    <div class="table-responsive">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
						<td>Nº de Pedido</td>
	                    <td>Fecha de compra</td>
	                    <td>Estado</td>
	                </tr>
	            </thead>
           		<tbody>
				   <?php
						while($d = mysqli_fetch_array($misPedidos)){
							echo "<tr>";
							echo "<td class='text-center'>".$d["NPedido"]."</td>";
							echo "<td class='text-center'>".$d["Fecha"]."</td>";
							echo "<td class='text-center'>".$d["Estado"]."</td>";
							echo "<td class='text-center'>
												<a class='btn btn-outline-primary btn-sm' href='profile.php?historial=2&npedido=".$d['NPedido']."'>Detalles</a>
											</td>";
							echo "<tr>";
						}
						?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>


