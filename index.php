<!DOCTYPE htlm>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.10.4/mercadolibre/favicon.svg" rel="icon" data-head-react="true">
	<script src="JS/scripts.js"></script>
    <title>Index</title>
	<?php 
		include("includes/menu.php");
		include("carrito/carrito_sql.php");
		if(isset($_SESSION['activo'])){
			if(!$_SESSION['activo']){
				session_unset();
				header('location: banned.php');
			}
		} 
	 ?>
    <link rel="stylesheet" href="CSS/styles.css">
</head>
<body>
	<header>
		<?php menu(); ?>
	</header>
	<div>
		<div class="alert alert-warning" >
			<h5 class="text-center">Bienvenido al carrito web.</h5>
		</div>
	</div>
	<section class="container">
			<?php 
				if(isset($_SESSION['user'])){
					echo "<div class='font-italic text-center'>
							<h6>Lista de productos disponibles</h6>
						</div>";
				}
				else{
					echo "<div class='font-italic text-center mt-3 mb-3'>
							<h6>Para poder realizar una compra, debe <a href='login.php'>logearse</a> o <a href='registro.php'>registrarse</a>.</h6>
						</div>";
				}
			?>
		<div class="container-fluid row d-flex">
			<div class="col-3"></div>
			<div class="table-responsive col-6">
	        <table class="table table-bordered table-sm table-hover mb-0">
	            <thead>
	                <tr style="background:#003325;color:white" class="text-center">
	                    <td>Productos</td>
	                    <td>Precio</td>
	                    <td>Stock</td>
	                </tr>
	            </thead>
           		<tbody>
				   <?php 
						while($d = mysqli_fetch_array($listaProductos)){
							echo "<tr>";
							echo "<td>".$d["Nombre"]."</td>";
							echo "<td class='text-center'>".$d["Precio"]."</td>";
							if($d["Cantidad"]>0){
								echo "<td class='text-info text-center'>Disponible</td>";
								if(isset($_SESSION['user']) && isset($_SESSION['listaCodigos'])){
									$match = false;
									foreach ($_SESSION['listaCodigos'] as $valor) {
										if($valor == $d['Codigo']){
											echo "<td class='text-center'>
													<a class='btn btn-outline-success btn-sm' href='#'>Ya agregado</a>
												</td>";
											$match = true;
											break;
										}
									}
									if(!$match){
										echo "<td class='text-center'>
												<a class='btn btn-outline-primary btn-sm' href='carrito/carrito.php?codigo=".$d['Codigo']."'>Agregar al carrito</a>
											</td>";
									}
								}
							}
							else{
								echo "<td class='text-danger text-center'>Sin Stock</td>";
								if(isset($_SESSION['user'])){
									echo "<td class='text-center'>
													<a class='btn btn-outline-danger btn-sm' href='#'>Sin Stock</a>
												</td>";
								}
							}
							echo "</tr>";
						}
					?>
	            </tbody>
	        </table>
	        <div class="text-right mt-4 mb-5">
	        	<a class="btn btn-outline-primary" href="profile.php?carrito=1">Continuar con la compra!</a>
	        </div>
	    	</div>
	    	<div class="col-3"></div>
		</div>
	</section>




</body>
</html>