<!DOCTYPE htlm>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.10.4/mercadolibre/favicon.svg" rel="icon" data-head-react="true">
    <title>Registrarse</title>
    <?php include("includes/menu.php"); ?>
    <link rel="stylesheet" href="CSS/styles.css">

    <!-- OBTENEMOS LOS MENSAJES DE ERROR/EXITO -->
	<?php 
		// SI ESTA LOGEADO NO LO DEJAMOS VOLVER A INGRESAR.
		if(isset($_SESSION['user'])){
			header("Location: index.php");
		}
        $mensaje="";
        if(isset($_GET["msg"])){
            $msg = $_GET["msg"];
            switch ($msg) {
                case "1": $mensaje = "<h5 class='text-success'>El usuario se creó correctamente.</h5>";
                    break;
                case "2": $mensaje = "<h5 class='text-danger'>El email ingresado ya existe.</h5>";
                    break;
                case "3": $mensaje = "<h5 class='text-danger'>Ya existe un usuario con ese nombre.</h5>";
                    break;
                case "4": $mensaje = "<h5 class='text-danger'>Las contraseñas no coinciden.</h5>";
                    break;
                case "5": $mensaje = "<h5 class='text-danger'> El email ingresado no es válido.</h5>";
                    break;
                case "6": $mensaje = "<h5 class='text-danger'>Los campos deben tener entre 5 y 10 caracteres únicamente alfanumericos.</h5>";
                    break;    
                default:
                    break;
            }
        }
    ?>
</head>
<body>
	<header>
		<?php menu(); ?>
    </header>
	<div>
		<div class="alert alert-warning mb-3"> 
			<h5 class="text-center">Registrar nueva cuenta.</h5>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row d-flex mt-3 mb-2">
			<div class="col-12">			
				<!-- MENSAJE DE EXITO/ERROR AL REGISTRAR -->
				<div class="text-center mb-3">
					<?php echo $mensaje; ?>
				</div>
			</div>
			<div class="col-4"></div>
				<div class="col-4 d-flex justify-content-center">
					<form action="registro_sql.php" method="POST">
						<div class="form-group">
							<label>Usuario:<input class="form-control" type="text" name="user" min="5" max="10"/></label>
						</div>
						<div class="form-group">
							<label>Nick: <input class="form-control" type="text" name="nick" min="5" max="10"/></label>
						</div>
						<div class="form-group">
							<label>Email: <input class="form-control" type="email" name="email" max="60"/></label>
						</div>
						<div class="form-group">
							<label>Contraseña: <input class="form-control" type="password" name="pass" min="5" max="10"/></label>
						</div>
						<div class="form-group">
							<label>Repetir Contraseña: <input class="form-control" type="password" name="pass2" min="5" max="10"/></label> 
						</div>
						<button type="submit" class="btn btn-primary form-control">Registrarse</button>
					</form>
						</div>
					<div class="col-4"></div>
				<div>
			</div>
		</div>
	</div>
</body>
</html>
