-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2020 a las 20:44:15
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tp1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `NPedido` int(11) NOT NULL,
  `NCliente` int(11) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `Estado` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`NPedido`, `NCliente`, `Fecha`, `Estado`) VALUES
(29, 4, '2020-11-16 12:25:22', 'Solicitado'),
(30, 4, '2020-11-16 14:43:51', 'Solicitado'),
(31, 1, '2020-11-20 12:27:18', 'Solicitado'),
(32, 1, '2020-11-20 12:32:24', 'Solicitado'),
(33, 1, '2020-11-20 12:32:56', 'Solicitado'),
(34, 1, '2020-11-20 12:35:29', 'Solicitado'),
(35, 1, '2020-11-20 12:38:24', 'Solicitado'),
(36, 1, '2020-11-20 12:38:51', 'Solicitado'),
(37, 1, '2020-11-20 12:40:28', 'Solicitado'),
(38, 7, '2020-11-20 12:42:28', 'Solicitado'),
(39, 7, '2020-11-20 12:49:50', 'Solicitado'),
(40, 7, '2020-11-20 12:50:07', 'Solicitado'),
(41, 7, '2020-11-20 13:13:08', 'Solicitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos_detalles`
--

CREATE TABLE `pedidos_detalles` (
  `NItem` int(11) NOT NULL,
  `NPedido` int(11) NOT NULL,
  `Codigo` int(11) NOT NULL,
  `Producto` varchar(255) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `PrecioUnidad` decimal(10,0) NOT NULL,
  `Total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos_detalles`
--

INSERT INTO `pedidos_detalles` (`NItem`, `NPedido`, `Codigo`, `Producto`, `Cantidad`, `PrecioUnidad`, `Total`) VALUES
(22, 32, 22, 'bordeadora', 1, '500', '500'),
(23, 33, 22, 'bordeadora', 1, '500', '500'),
(24, 34, 22, 'bordeadora', 3, '500', '1500'),
(25, 36, 22, 'bordeadora', 10, '500', '5000'),
(26, 37, 22, 'bordeadora', 10, '500', '5000'),
(27, 38, 13, 'tenaza grande', 1, '321', '321'),
(28, 38, 15, 'madera larga', 80, '500', '40000'),
(29, 38, 16, 'carretilla mediana', 1, '680', '680'),
(30, 38, 27, 'tenaza', 2, '250', '500'),
(31, 39, 17, 'carretilla grande', 1, '1200', '1200'),
(32, 40, 17, 'carretilla grande', 15, '1200', '18000'),
(33, 41, 15, 'madera larga', 1, '500', '500'),
(34, 48, 17, 'carretilla grande', 1, '1200', '1200');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Precio` decimal(10,0) NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`Codigo`, `Nombre`, `Precio`, `Cantidad`) VALUES
(13, 'tenaza grande', '321', 443),
(15, 'madera larga', '500', 918),
(16, 'carretilla mediana', '680', 299),
(17, 'carretilla grande', '1200', 383),
(27, 'tenaza', '250', 1998),
(29, 'tornillos x 1kg', '200', 499),
(30, 'pala ancha', '500', 111),
(31, 'tornillos x 2kg', '500', 111),
(32, 'producto sin stock', '11', 0),
(33, 'bordeadora', '6582', 666);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(11) NOT NULL,
  `Usuario` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `Nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `Activo` tinyint(1) DEFAULT NULL,
  `Saldo` decimal(10,0) NOT NULL DEFAULT 0,
  `isAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `FechaAlta` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `Usuario`, `Password`, `Nick`, `Email`, `Activo`, `Saldo`, `isAdmin`, `FechaAlta`) VALUES
(1, 'Lucas', '123123', 'Lucas', 'lucasemilino24@hotmail.com', 1, '2223', 1, '2020-10-14 00:00:00'),
(3, 'Lucas1', '12345', 'Lucas1', 'LucasEmilino22@hotmail.com', 1, '9002', 0, '2020-11-12 00:00:00'),
(4, 'Lucas3', '12345', 'Lucas3', 'LucasEmilino23@hotmail.com', 0, '0', 0, '2020-11-12 00:00:00'),
(5, 'noadmin', 'noadmin', 'noadmin', 'noadmin@gmail.com', 1, '1600', 0, '2020-11-12 00:00:00'),
(6, 'FABIANO', '123123', 'Caruana', 'fabianocaruana@holamundo.com', 0, '2504', 0, '2020-11-12 00:00:00'),
(7, 'HIKARU', '123123', 'Nakamura', 'hikarunakamura@gmail.com', 1, '59800', 0, '2020-11-12 00:00:00'),
(8, 'LIREN', '123123', 'Ding1', 'dingliren@yahoo.com.ar', 1, '0', 0, '2020-11-12 00:00:00'),
(9, 'NOACTIVO', 'noactivo', 'noactivo', 'noactive@noactive.com', 0, '2', 0, '2020-11-12 00:00:00'),
(12, 'LUCASES', 'Lucases', 'Lucases', 'lucasemiliano21@hotmail.com', 0, '627', 0, '2020-11-12 00:00:00'),
(13, 'AGUANTEPHP', 'aguantephp', 'aguantephp', 'aguantephp@php.com', 1, '0', 0, '2020-11-12 00:00:00'),
(16, 'JUANPABLO', '123123', 'segundo', 'juancarlos@segundo.com', 1, '0', 0, '2020-11-12 00:00:00'),
(17, 'PEPITOXD', 'pepitoxd', 'pepitoxd', 'pepitoxd@gmail.com', 1, '0', 0, '2020-11-12 00:00:00'),
(18, 'PEPITOXD2', 'pepitoxd2', 'pepitoxd2', 'pepitoxd2@gmail.com', 1, '0', 0, '2020-11-12 00:00:00'),
(19, 'GUILLERMO', 'guillermo', 'franchela', 'guillefranchela@abc.com', 1, '0', 0, '2020-11-12 00:00:00'),
(20, 'TIMESTAP', 'timestap', 'timestap', 'timestap@prueba.com', 1, '0', 0, '2020-11-12 11:31:39'),
(21, 'ADMIN', 'admin', 'admin', 'admin@admin.com', 1, '500', 1, '2020-11-12 11:32:22');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`NPedido`);

--
-- Indices de la tabla `pedidos_detalles`
--
ALTER TABLE `pedidos_detalles`
  ADD PRIMARY KEY (`NItem`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Usuario` (`Usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `NPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `pedidos_detalles`
--
ALTER TABLE `pedidos_detalles`
  MODIFY `NItem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
