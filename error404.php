<!DOCTYPE htlm>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.10.4/mercadolibre/favicon.svg" rel="icon" data-head-react="true">
    <title>Pagina no encontrada!</title>
    <?php include("includes/menu.php"); ?>
    <link rel="stylesheet" href="CSS/styles.css">
</head>
<body>
	<header>
		<?php menu(); ?>
	</header>
	<div>
		<div class="alert alert-warning" >
			<h5 class="text-center">Lo sentimos, aquí no hay nada para mostrar.</h5>
		</div>
	</div>
	<section class="container">
		<div class="text-center">
			<img alt="error xd" src="https://www.gstatic.com/youtube/src/web/htdocs/img/monkey.png">
		</div>
	</section>

</body>
</html>

	